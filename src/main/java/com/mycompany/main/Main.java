/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.main;

/**
 *
 * @author ATHLON
 */
import java.util.ArrayList;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        RegistroAutomoviles registro = new RegistroAutomoviles();

        while (true) {
            System.out.println("----- MENÚ -----");
            System.out.println("1. Registrar automóvil");
            System.out.println("2. Listar automóviles");
            System.out.println("3. Buscar automóvil");
            System.out.println("4. Eliminar automóvil");
            System.out.println("5. Salir");
            System.out.print("Ingrese la opción deseada: ");
            int opcion = scanner.nextInt();
            scanner.nextLine();

            switch (opcion) {
                case 1:
                    System.out.print("Ingrese la placa del automóvil: ");
                    String placa = scanner.nextLine();
                    System.out.print("Ingrese el color del automóvil: ");
                    String color = scanner.nextLine();
                    System.out.print("Ingrese el modelo del automóvil: ");
                    String modelo = scanner.nextLine();
                    System.out.print("Ingrese el año del automóvil: ");
                    int anio = scanner.nextInt();
                    scanner.nextLine();
                    System.out.print("Ingrese el número de puertas del automóvil: ");
                    int numeroPuertas = scanner.nextInt();
                    scanner.nextLine();
                    System.out.print("Ingrese la fecha de compra del automóvil: ");
                    String fechaCompra = scanner.nextLine();

                    System.out.print("Ingrese el nombre completo del dueño: ");
                    String nombreCompleto = scanner.nextLine();
                    System.out.print("Ingrese el apellido del dueño: ");
                    String apellido = scanner.nextLine();
                    System.out.print("Ingrese el departamento de nacimiento del dueño: ");
                    String departamentoNacimiento = scanner.nextLine();
                    System.out.print("Ingrese el año de nacimiento del dueño: ");
                    int anioNacimiento = scanner.nextInt();
                    scanner.nextLine();

                    Duenio duenio = new Duenio(nombreCompleto, apellido, departamentoNacimiento, anioNacimiento);
                    Automovil automovil = new Automovil(placa, color, modelo, anio, numeroPuertas, fechaCompra, duenio);
                    registro.agregarAutomovil(automovil);
                    break;
                case 2:
                    registro.listarAutomoviles();
                    break;
                case 3:
                    System.out.print("Ingrese la placa del automóvil a buscar: ");
                    String placaBuscar = scanner.nextLine();
                    registro.buscarAutomovil(placaBuscar);
                    break;
                case 4:
                    System.out.print("Ingrese la placa del automóvil a eliminar: ");
                    String placaEliminar = scanner.nextLine();
                    registro.eliminarAutomovil(placaEliminar);
                    break;
                case 5:
                    System.out.println("¡Hasta luego!");
                    System.exit(0);
                default:
                    System.out.println("Opción inválida. Por favor, ingrese una opción válida.");
            }

            System.out.println();
        }
    }
}
class Automovil {
    private String placa;
    private String color;
    private String modelo;
    private int anio;
    private int numeroPuertas;
    private String fechaCompra;
    private Duenio duenio;

    public Automovil(String placa, String color, String modelo, int anio, int numeroPuertas, String fechaCompra, Duenio duenio) {
        this.placa = placa;
        this.color = color;
        this.modelo = modelo;
        this.anio = anio;
        this.numeroPuertas = numeroPuertas;
        this.fechaCompra = fechaCompra;
        this.duenio = duenio;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public void setNumeroPuertas(int numeroPuertas) {
        this.numeroPuertas = numeroPuertas;
    }

    public void setFechaCompra(String fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public void setDuenio(Duenio duenio) {
        this.duenio = duenio;
    }

    public String getPlaca() {
        return placa;
    }

    public String getColor() {
        return color;
    }

    public String getModelo() {
        return modelo;
    }

    public int getAnio() {
        return anio;
    }

    public int getNumeroPuertas() {
        return numeroPuertas;
    }

    public String getFechaCompra() {
        return fechaCompra;
    }

    public Duenio getDuenio() {
        return duenio;
    }

    public void verAutomovil() {
        System.out.println("Placa: " + placa);
        System.out.println("Modelo: " + modelo);
        System.out.println("Color: " + color);
    }

    public void verDuenio() {
        System.out.println("Placa del automóvil: " + placa);
        System.out.println("Modelo del automóvil: " + modelo);
        System.out.println("Nombre completo del dueño: " + duenio.getNombreCompleto());
    }
}

class Duenio {
    private String nombreCompleto;
    private String apellido;
    private String departamentoNacimiento;
    private int anioNacimiento;

    public Duenio(String nombreCompleto, String apellido, String departamentoNacimiento, int anioNacimiento) {
        this.nombreCompleto = nombreCompleto;
        this.apellido = apellido;
        this.departamentoNacimiento = departamentoNacimiento;
        this.anioNacimiento = anioNacimiento;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setDepartamentoNacimiento(String departamentoNacimiento) {
        this.departamentoNacimiento = departamentoNacimiento;
    }

    public void setAnioNacimiento(int anioNacimiento) {
        this.anioNacimiento = anioNacimiento;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public String getApellido() {
        return apellido;
    }

    public String getDepartamentoNacimiento() {
        return departamentoNacimiento;
    }

    public int getAnioNacimiento() {
        return anioNacimiento;
    }
}

class RegistroAutomoviles {
    private ArrayList<Automovil> automoviles;

    public RegistroAutomoviles() {
        automoviles = new ArrayList<>();
    }

    public void agregarAutomovil(Automovil automovil) {
        automoviles.add(automovil);
        System.out.println("Automóvil registrado con éxito.");
    }

    public void listarAutomoviles() {
        for (Automovil automovil : automoviles) {
            System.out.println("Placa: " + automovil.getPlaca());
            System.out.println("Modelo: " + automovil.getModelo());
            System.out.println("Color: " + automovil.getColor());
            System.out.println("-------------------------");
        }
    }

    public void buscarAutomovil(String placa) {
        for (Automovil automovil : automoviles) {
            if (automovil.getPlaca().equals(placa)) {
                automovil.verAutomovil();
                return;
            }
        }
        System.out.println("No se encontró un automóvil con la placa proporcionada.");
    }

    public void eliminarAutomovil(String placa) {
        for (int i = 0; i < automoviles.size(); i++) {
            Automovil automovil = automoviles.get(i);
            if (automovil.getPlaca().equals(placa)) {
                automoviles.remove(i);
                System.out.println("Automóvil eliminado con éxito.");
                return;
            }
        }
        System.out.println("No se encontró un automóvil con la placa proporcionada.");
    }
}


    

